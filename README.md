# BRIGHTIDEA FRONT END TECHNICAL CHALLENGE
### TASK
Using the JavaScript framework of your choice build a small app to search and display the weather in different cities using the http://openweathermap.org API. The challenge should not take more than 4-5 hours to complete.

REQUIRED
* Create your own public GitHub repository so you can commit and push regularly (or zip the files but include a changelog)
* Use SASS or LESS for your CSS
* Incorporate responsive design
* Use build tools to concat / minify your files

BONUS
* Leverage the browsers storage & caching mechanisms by saving previous queries
* Animate the weather icons using CSS3 or D3js
* Surprise us!


***************************************************************************************
##devlopment tools and result

Tools:
- Public Git repository used;
- LESS applied;
- React.js, D3.js applied;
- Gulp and browserify used to build and minify files;

Features implemented:
- simple responsive design;
- google auto complete for city and weather search;
- current weather details displayed in a table;
- bar chart to display weather forecast;
- hover to bar chart shows current bar's time and weather;
- animation on weather icon;

***************************************************************************************
##Weather check

#local dev environment setup

You must have [npm](https://www.npmjs.org) installed on your computer.
From the root project directory run these commands from the command line:

    npm install

To build the project, run this command:

    npm start

This will perform an initial build and start a watcher process that will update bundle.js with any changes you wish to make.

To run the app, simply open the index.html file in a browser.