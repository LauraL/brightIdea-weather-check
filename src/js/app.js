import React from 'react';
import ReactDOM from 'react-dom';
import Weather from './components/Weather';


ReactDOM.render(
  <div>
    <Weather />
  </div>,
    document.getElementById('example')
);
