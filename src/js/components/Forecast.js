import React from 'react';
var ReactD3 = require('react-d3-components');
var d3 = require('d3');
var BarChart = ReactD3.BarChart;


class Chart extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        list: props.list
      };
      this.tooltip = this.tooltip.bind(this);
    }

  componentDidUpdate(prevProps, prevState) {
  if (prevProps.list !== this.props.list) {
    this.setState({list: this.props.list});
   }
  }

  tooltip (x, y1, y) {
    return  "time: "+ x + " temperature: " + y;
  };

  render() {
     //create new array for bar chart
     if (this.state.list.length > 0) {
       var _data = this.state.list.map(function (item) {
         var obj = {};
         var day =item.dt_txt.split(' ')[0].split('-');
         day = day[1].concat('/'+day[2]);
         var hour = item.dt_txt.split(' ')[1].slice(0, 5);

         obj['x'] = day + ' ' + hour;
         obj['y'] = Math.round(item.main.temp - 272.15);
         return obj;
       });
       var data = [{label:'time', values: _data}];

       return(
         <div>
           <h1>Forecasts</h1>
           <h1>{_data[0].time}</h1>
            <BarChart
                   data={data}
                   width={window.innerWidth*0.6}
                   height={400}
                   margin={{top: 10, bottom: 50, left: 50, right: 20}}
                   tooltipHtml={this.tooltip}
                   yAxis={{label: "Temperature (C)"}}
                   xAxis={{label: "Time (m/d t)"}}
                />
         </div>
       );
     }else {
       return (
         <div></div>
       );
     }
  }
}


export default Chart;
