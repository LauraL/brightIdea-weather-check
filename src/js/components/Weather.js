import React from 'react';
import Autocomplete from 'react-google-autocomplete';
import Chart from './Forecast';


class Weather extends React.Component {
  constructor(props) {
    super(props);
    this.state= {
      input: '',
      name:'',
      weather: '',
      img: null,
      humidity: '',
      forecast: {},
      pressure: '',
      wind: '',
      description: ''
    };
    this.updateInput = this.updateInput.bind(this);
    this.updateWeather = this.updateWeather.bind(this);
  }

  updateInput(e) {
     this.setState({input: e.formatted_address});
  }

  updateWeather(e) {
    e.preventDefault();
    this.setState({result: ''});
    var _self = this;
    $.ajax({
      dataType: "json",
      url: 'http://api.openweathermap.org/data/2.5/weather?q=' + this.state.input + '&APPID=5ccb0f4e53e3da0d98098428f1286e9c',
      method: 'GET',
      success: function (result) {
        _self.setState({
                        weather: Math.round(result.main.temp - 272.15) + ' C',
                        img: 'http://openweathermap.org/img/w/' + result.weather[0].icon + '.png',
                        pressure: result.main.pressure + ' hpa',
                        humidity: result.main.humidity + '%',
                        wind: result.wind.speed + ' m/s',
                        description: result.weather[0].description,
                        name: result.name
                        });
      }
    });

    $.ajax({
      dataType: "json",
      url: 'http://api.openweathermap.org/data/2.5/forecast?q=' + this.state.input + '&APPID=5ccb0f4e53e3da0d98098428f1286e9c',
      method: 'GET',
      success: function (result) {
        _self.setState({forecast: result.list.slice(0, 10)});
      }
    });
  }

  render() {
    if (this.state.pressure) {
      return (
        <div>
          <div>
            <h1>Weather Check</h1>
            <p>Please enter city name or zip code</p>
            <div className="input">
              <Autocomplete
                style={{width: '100%'}}
                onPlaceSelected={(e)=>this.updateInput(e)}
                types={['(regions)']}
              />
            </div>
            <button onClick={this.updateWeather}><h4>Check</h4></button>
            <div id="weather">
              <p>Weather in {this.state.name}</p>
              <img className="icon" src={this.state.img} alt=""/>
              <h1 className="temp">{this.state.weather}</h1>
              <p>{this.state.description}</p>
              <table>
                <tbody>
                  <tr>
                    <td>Pressure</td>
                    <td>{this.state.pressure}</td>
                  </tr>
                  <tr>
                    <td>Humidity</td>
                    <td>{this.state.humidity}</td>
                  </tr>
                  <tr>
                    <td>Wind</td>
                    <td>{this.state.wind}</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <div id="forecast">
            <Chart list={this.state.forecast}/>
          </div>
        </div>
      );
    }else return (
      <div>
        <h1>Weather Check</h1>
        <p>Please enter city name or zip code</p>
        <div className="input">
          <Autocomplete
            style={{width:'100%'}}
            onPlaceSelected={(e)=>this.updateInput(e)}
            types={['(regions)']}
          />
          </div>
        <button onClick={this.updateWeather}><h4>Check</h4></button>
      </div>
    );
  }
}


export default Weather;
