var gulp = require('gulp');

// for JS building
var browserify = require('browserify');
var watchify = require('watchify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var minify = require('gulp-minifier');

// for CSS building
var cleanCSS = require('gulp-clean-css');
var concat = require('gulp-concat');
var less = require('gulp-less');

// for deployment
var git = require('git-rev-sync');

var version = git.short();

var getWorkDir = function (role) {
  return !!role ? './dist/' + role : './build';
};

// Build index.html
var buildHtml = function (role) {
  return gulp.src('./src/index.html')
    .pipe(gulp.dest(getWorkDir(role)));
};

// Build CSS files
var buildCss = function (role) {
  var dir = !!role ? '/static/' + version + '/css' : '/static/css';
  return gulp.src('./src/static/style/**/*')
    .pipe(less())
    .pipe(concat('bundle.min.css'))
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest(getWorkDir(role) + dir));
};

// Build JS (static) files
var buildStaticJs = function (role) {
  var dir = !!role ? '/static/' + version + '/js' : '/static/js';
  return gulp.src('./src/static/js/**/*')
    .pipe(gulp.dest(getWorkDir(role) + dir));
};

// Watch JS files
var watchJs = function (src) {
  var watcher  = watchify(browserify({
    entries: ['./src/js/' + src + '.js'],
    debug: true,
    cache: {},
    packageCache: {},
    fullPaths: true
  }));

  return watcher.on('update', function () {
      watcher
        .transform('babelify', {presets: ['es2015', 'react']})
        .bundle()
        .pipe(source(src + '.min.js'))
        .pipe(buffer())
        .pipe(minify({
          minify: true,
          collapseWhitespace: true,
          conservativeCollapse: true,
          minifyJS: true,
          minifyCSS: true,
          getKeptComment: function (content, filePath) {
              var m = content.match(/\/\*![\s\S]*?\*\//img);
              return m && m.join('\n') + '\n' || '';
          }
        }))
        .pipe(gulp.dest(getWorkDir() + '/static/js'));
    })
    .transform('babelify', {presets: ['es2015','react']})
    .bundle()
    .pipe(source(src + '.min.js'))
    .pipe(buffer())
    .pipe(minify({
      minify: true,
      collapseWhitespace: true,
      conservativeCollapse: true,
      minifyJS: true,
      minifyCSS: true,
      getKeptComment: function (content, filePath) {
          var m = content.match(/\/\*![\s\S]*?\*\//img);
          return m && m.join('\n') + '\n' || '';
      }
    }))
    .pipe(gulp.dest(getWorkDir() + '/static/js'));
};

// Tasks - watch
gulp.task('watchJs', function () {
  watchJs('app');
});

gulp.task('watch',
  ['buildHtml', 'watchJs', 'buildCss', 'buildStaticJs'],
  function () {
    gulp.watch('./src/static/style/**/*', ['buildCss']);
    gulp.watch('./src/static/js/**/*', ['buildStaticJs']);
  }
);


// Tasks - build
gulp.task('buildHtml', function () { return buildHtml(); });
gulp.task('buildCss', function () { return buildCss(); });
gulp.task('buildStaticJs', function () { return buildStaticJs(); });


// Default
gulp.task('default', ['watch']);
