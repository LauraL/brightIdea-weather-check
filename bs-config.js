module.exports = {
  open: false,
  server: {
    baseDir: './build'
  },
  files: './build/**/*'
};
